<?php

namespace Agrekom\Contact\Setup;

class InstallData implements \Magento\Framework\Setup\InstallDataInterface
{

    /**
     * @var \Magento\Cms\Model\BlockFactory
     */
    private $blockFactory;

    /**
     * @var \Magento\Cms\Model\ResourceModel\Block
     */
    private $blockResource;

    /**
     * @var array
     */
    private $blocks;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    public function __construct(
        \Magento\Cms\Model\BlockFactory $blockFactory,
        \Magento\Cms\Model\ResourceModel\Block $blockResource,
        \Psr\Log\LoggerInterface $logger
    )
    {
        $this->blockFactory = $blockFactory;
        $this->blockResource = $blockResource;
        $this->blocks = [];
        $this->logger = $logger;
    }

    public function install(\Magento\Framework\Setup\ModuleDataSetupInterface $setup, \Magento\Framework\Setup\ModuleContextInterface $context)
    {
        try {
            $this->installBlocks();
        } catch (\Exception $e) {
            $this->logger->log(\Psr\Log\LogLevel::NOTICE, $e->getMessage());
        }
    }

    private function installBlocks()
    {
        $this->createContactUsBlock();

        foreach ($this->blocks as $block) {
            $this->saveBlock($block);
        }
    }

    private function createContactUsBlock()
    {
        $content = <<<'EOT'
        <h2>CHCESZ ZŁOŻYĆ ZAMÓWIENIE?</h2>
        <h2>NASZ DZIAŁ HANDLOWY DORADZI PODCZAS WYBORU AGREGATU</h2>
        <p>&nbsp;</p>
        <h3><strong>AGREGATY TYNKARSKIE</strong></h3>
        <h4>Łukasz tel: 795 786 888</h4>
        <h3><strong>AGREGATY MALARSKIE I BIELARKI</strong></h3>
        <h4>Maciej tel: 791 002 003</h4>
        <p>&nbsp;</p>
        <h3>Jeżeli masz pytania dotyczące działania naszych urządzeń, skontaktuj się działem pomocy technicznej:</h3>
        <h4>Tomasz tel: 533 008 002</h4>
        <p>&nbsp;</p>
        <p><strong>lub napisz do nas:&nbsp;<a href="mailto:biuro@agrekom.pl">biuro@agrekom.pl</a></strong></p>
        <p>&nbsp;</p>
        <p><strong>ADRES BIURA:</strong></p>
        <p><strong>Al. Wolności 22</strong></p>
        <p><strong>41-219 Sosnowiec</strong></p>
EOT;

        $block = $this->blockFactory->create();
        $block->setData([
            'title' => 'Contact Us',
            'identifier' => 'contact-us',
            'content' => $content,
            'stores' => [0],
            'is_active' => 1,
        ]);
        $this->blocks[] = $block;
    }

    private function saveBlock(\Magento\Cms\Model\Block $block)
    {
        $identifier = $block->getIdentifier();

        if ($this->blockExists($identifier)) {
            return;
        }

        $this->blockResource->save($block);
    }

    private function blockExists($identifier)
    {
        $block = $this->blockFactory->create();
        $this->blockResource->load($block, $identifier, 'identifier');

        if ($block->getId()) {
            return true;
        }

        return false;
    }

}
